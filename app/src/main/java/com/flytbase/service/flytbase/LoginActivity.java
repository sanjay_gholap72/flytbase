package com.flytbase.service.flytbase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import br.com.simplepass.loadingbutton.customViews.CircularProgressButton;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener
{
    EditText edtPhoneNumber;
    CircularProgressButton btnNext;

    String phoneNumber;
    String phonePattern = "^[0-9]{10}$";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtPhoneNumber = findViewById(R.id.edit_phonenumber);
        btnNext = findViewById(R.id.btn_next);

        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v == btnNext)
        {
            Login();
        }
    }



    private void Login()
    {
        phoneNumber = edtPhoneNumber.getText().toString().trim();

        if(TextUtils.isEmpty(phoneNumber))
        {
            edtPhoneNumber.setError("Field cannot be left blank.");
        }
        else if (!phoneNumber.matches(phonePattern))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Please enter valided phone number")
                    .show();
        }
        else
        {
            phoneNumber = "+91"+phoneNumber;

            Intent intent = new Intent(LoginActivity.this,AuthenticationLoginActivity.class);
            intent.putExtra("phoneNumber",phoneNumber);
            startActivity(intent);
        }
    }
}