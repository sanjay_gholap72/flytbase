package com.flytbase.service.flytbase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.udojava.evalex.Expression;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btn1, btn2, btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn0;
    Button btnplus,btnminus,btnmult,btndiv;
    Button btnequal,btnclr;
    Button btnHistory;

    EditText edtInput;
    TextView txtResult;

    String strEdt;

    List<String> history;
    int i = 0;


    @Override

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = findViewById(R.id.but1);
        btn2 = findViewById(R.id.but2);
        btn3 = findViewById(R.id.but3);
        btn4 = findViewById(R.id.but4);
        btn5 = findViewById(R.id.but5);
        btn6 = findViewById(R.id.but6);
        btn7 = findViewById(R.id.but7);
        btn8 = findViewById(R.id.but8);
        btn9 = findViewById(R.id.but9);
        btn0 = findViewById(R.id.butzero);

        btnplus = findViewById(R.id.butadd);
        btnminus = findViewById(R.id.butmin);
        btnmult = findViewById(R.id.butmul);
        btndiv = findViewById(R.id.butdiv);

        btnequal = findViewById(R.id.butequal);
        btnclr = findViewById(R.id.butclear);

        edtInput = findViewById(R.id.input_box);
        txtResult = findViewById(R.id.result_box);

        btnHistory = findViewById(R.id.btnhistory);

        history = new ArrayList<>();


        btn1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "1");
            }
        });

        btn2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "2");
            }
        });

        btn3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "3");
            }
        });

        btn4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "4");
            }
        });

        btn5.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "5");
            }
        });

        btn6.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "6");
            }
        });

        btn7.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "7");
            }
        });

        btn8.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "8");
            }
        });

        btn9.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "9");
            }
        });

        btn0.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "0");
            }
        });

        btnminus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "-");

            }
        });

        btnmult.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "*");
            }
        });

        btndiv.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "/");
            }
        });

        btnplus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(edtInput.getText() + "+");
            }
        });

        btnclr.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               strEdt = edtInput.getText().toString().trim();

                StringBuffer sb= new StringBuffer(strEdt);
                sb.deleteCharAt(sb.length()-1);

                edtInput.setText(sb);
            }
        });


        btnequal.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String result,temp;

                strEdt = edtInput.getText().toString().trim();

                if(TextUtils.isEmpty(strEdt))
                {
                    Toast.makeText(MainActivity.this, "Enter number",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    result = String.valueOf(EvaluateString.evaluate(strEdt));
                    txtResult.setText(result);

                    temp = strEdt+" = "+result;

                    Log.wtf("temp",temp);

                    history.add(temp);
                }


            }
        });


        btnHistory.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                edtInput.setText(null);
                int count = history.size();

                if(count == 0)
                {
                    Toast.makeText(MainActivity.this, "No History",Toast.LENGTH_SHORT).show();
                }
                else
                {
                      if(i<count)
                      {
                          txtResult.setText(history.get(i));

                          i = i+1;
                      }
                }
            }
        });

    }
}